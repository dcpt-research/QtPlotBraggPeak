/*! \file
    General purpose mathematical or physical %constants.
 */

#ifndef SCITBX_CONSTANTS_H
#define SCITBX_CONSTANTS_H

namespace scitbx {

//! General purpose mathematical or physical %constants.
namespace constants {

constexpr double atan_one = 0.7853981634;
//! mathematical constant pi
constexpr double pi = 4. * atan_one;
//! mathematical constant pi*pi
constexpr double pi_sq = pi * pi;
//! mathematical constant 2*pi
constexpr double two_pi = 8. * atan_one;
//! mathematical constant 2*pi*pi
constexpr double two_pi_sq = 2. * pi_sq;
//! mathematical constant 4*pi
constexpr double four_pi = 16. * atan_one;
//! mathematical constant 4*pi*pi
constexpr double four_pi_sq = two_pi * two_pi;
//! mathematical constant 8*pi*pi
constexpr double eight_pi_sq = 2. * four_pi_sq;
//! mathematical constant pi/2
constexpr double pi_2 = 2. * atan_one;
//! mathematical constant pi/180
constexpr double pi_180 = atan_one / 45.;

constexpr double sqrt_pi = 1.77245385091;

constexpr double sqrt_2 = 1.41421356237;

constexpr double sqrt_2bypi = 0.7978845608;

//! Factor for keV <-> Angstrom conversion.
/*!
  http://physics.nist.gov/PhysRefData/codata86/table2.html

  h = Plank's Constant = 6.6260755e-34 J s
  c = speed of light = 2.99792458e+8 m/s
  1 keV = onee+3 * one60217733e-19 J
  1 A = Angstrom = onee-10 m

  E = (h * c) / lamda;

  Exponents: (-34 + 8) - (3 - 19 - 10) = 0
 */
constexpr double factor_kev_angstrom = 6.6260755 * 2.99792458 / 1.60217733;
constexpr double factor_ev_angstrom = 6626.0755 * 2.99792458 / 1.60217733;
} // namespace constants

//! Conversions from degrees to radians.
constexpr double deg_as_rad(const double deg) {
  return deg * constants::pi_180;
}
//! Conversions from radians to degrees.
constexpr double rad_as_deg(const double rad) {
  return rad / constants::pi_180;
}

} // namespace scitbx

#endif // SCITBX_CONSTANTS_H
