#include <QApplication>
#include <QtGui/QPainter>
#include <QtPrintSupport/QPrinter>

#include <algorithm> // transform, for_each, fill, generate
#include <array>     // array
#include <cmath>     // M_PI
#include <iostream>  // cout, cerr, endl, <<
#include <memory>    // unique_pointer
#include <numeric>   // accumulate
#include <string>    // string, <<
#include <valarray>  // valarray
#include <vector>    // vector

#include "paraboliccylinder.h"
#include "qcustomplot.h"

enum material_t { WATER, AIR };

constexpr auto n_points = 1000;
constexpr auto step_length = 400.0 / n_points; // 40 cm
constexpr auto init_energy = 200.0;            // MeV

constexpr auto start_air = 50.0;
constexpr auto end_air = 100.0;
//----------------------------------------------------------------------------
// natural logarithm using
// https://en.wikipedia.org/wiki/Natural_logarithm#High_precision
// domain error occurs if x <= 0
namespace detail {
// test whether values are within machine epsilon, used for algorithm
// termination
template <typename T> constexpr bool feq(T x, T y) {
  return abs(x - y) <= 8 * std::numeric_limits<T>::epsilon();
}
template <typename T> constexpr T log_iter(T x, T y) {
  return y + T{2} * (x - std::exp(y)) / (x + std::exp(y));
}
template <typename T> constexpr T log(T x, T y) {
  return feq(y, log_iter(x, y)) ? y : log(x, log_iter(x, y));
}
constexpr long double e() { return 2.71828182845904523536l; }
// For numerical stability, constrain the domain to be x > 0.25 && x < 1024
// - multiply/divide as necessary. To achieve the desired recursion depth
// constraint, we need to account for the max double. So we'll divide by
// e^5. If you want to compute a compile-time log of huge or tiny long
// doubles, YMMV.

// if x <= 1, we will multiply by e^5 repeatedly until x > 1
template <typename T> constexpr T logGT(T x) {
  return x > T{0.25} ? log(x, T{0})
                     : logGT<T>(x * e() * e() * e() * e() * e()) - T{5};
}
// if x >= 2e10, we will divide by e^5 repeatedly until x < 2e10
template <typename T> constexpr T logLT(T x) {
  return x < T{1024} ? log(x, T{0})
                     : logLT<T>(x / (e() * e() * e() * e() * e())) + T{5};
}

// exp by Taylor series expansion
template <typename T> constexpr T exp(T x, T sum, T n, int i, T t) {
  return feq(sum, sum + t / n) ? sum : exp(x, sum + t / n, n * i, i + 1, t * x);
}
} // namespace detail

const char *log_domain_error;
const char *exp_runtime_error;

template <typename FloatingPoint>
constexpr FloatingPoint ce_log(
    FloatingPoint x,
    typename std::enable_if<std::is_floating_point<FloatingPoint>::value>::type
        * = nullptr) {
  return x < 0 ? throw log_domain_error
               : x >= FloatingPoint{1024} ? detail::logLT(x) : detail::logGT(x);
}
template <typename Integral>
constexpr double
ce_log(Integral x,
       typename std::enable_if<std::is_integral<Integral>::value>::type * =
           nullptr) {
  return ce_log(static_cast<double>(x));
}

template <typename FloatingPoint>
constexpr FloatingPoint ce_exp(
    FloatingPoint x,
    typename std::enable_if<std::is_floating_point<FloatingPoint>::value>::type
        * = nullptr) {
  return true ? detail::exp(x, FloatingPoint{1}, FloatingPoint{1}, 2, x)
              : throw exp_runtime_error;
}
template <typename Integral>
constexpr double
ce_exp(Integral x,
       typename std::enable_if<std::is_integral<Integral>::value>::type * =
           nullptr) {
  return detail::exp<double>(x, 1.0, 1.0, 2, x);
}

template <typename FloatingPoint>
constexpr FloatingPoint ce_pow(
    FloatingPoint x, FloatingPoint y,
    typename std::enable_if<std::is_floating_point<FloatingPoint>::value>::type
        * = nullptr) {
  // x^y = exp(log(x))^y = exp(log(x)*y)
  return ce_exp(ce_log(x) * y);
}

#ifdef __GNUC__
// cannot do constexpr in clang as std::pow, std::exp, etc. is not implemented
// as constexpr
#define GCC_CONSTEXPR constexpr
#else
#define GCC_CONSTEXPR
#endif

GCC_CONSTEXPR double beta_squared(double T) {
  // Convert beam mass from AMU to MeV
  const double M_b = 1.0072766 * 931.4940954;

  // Beta^2
  return T * (T + 2.0 * M_b) / ((T + M_b) * (T + M_b));
}

// bethe for water and water equivalent
template <material_t material> GCC_CONSTEXPR double bethe(const double T) {
  const double pi = 3.1415926535897932384626433832795028841971693993751;
  const double N_A = 6.022140857e23; // mol^-1
  // const double mass_to_energy = 931.4940954; //MeV
  const double Z = 0.555086707; // Z/A of water
  double I = 67.2e-6;           // Exitation potential of water [MeV]

  // classical electron radius = e^2/(4*pi*eps * m_e)
  const double r = 2.8179403227e-15 * 100.0; // convert to cm
  const double m_e_x2 =
      0.5109989461 * 2.0; // MeV times two to shave off some instructions

  const double b_2 = beta_squared(T);

  // Beta^2 * Gamma^2
  const double b_2_x_g_2 = b_2 / (1.0 - b_2);

  const double first = 2.0 * pi * (r * r) * m_e_x2;

  // N_A [mol^-1]  Z/A of water
  auto n =
      N_A * Z; // * rho (g/cm3) / M_u (g/mol) -> 1/M_m = 1 (cm^-3) for water
  if (material == AIR) {
    // https://iopscience.iop.org/article/10.1088/0022-3727/33/18/310/meta
    n = 10e13; // cm^-3
    // https://dspace.library.uu.nl/bitstream/handle/1874/24230/bogaardt_koudijs_51_average_excitation_potentials_air_aluminium.pdf?sequence=1
    I = 7.7e-5; // MeV
  }
  const double second = n / b_2;

  const double m_e_x2_x_b_2_x_g_2_div_I = m_e_x2 * b_2_x_g_2 / I;
  const double third = ce_log(m_e_x2_x_b_2_x_g_2_div_I) - b_2;

  return first * second * third * 0.1; // MeV/mm
}

GCC_CONSTEXPR double BortfeldBraggPeak(const double R0, const double phi0,
                                       const double epsilon, const double sig,
                                       const double z) {
  // This is the very specialised equation for water only. It's equation 28/29
  // in the paper
  const double toGray = 1.602E-10;
  if (z < (R0 - 10 * sig)) {
    const auto fac = (phi0) / (1.0 + 0.012 * R0);
    const auto term1 = 17.93 * pow(R0 - z, -0.435);
    const auto term2 = ((0.444 + 31.7 * epsilon) / R0) * pow(R0 - z, 0.565);
    return fac * (term1 + term2) * toGray;
  } else if (z < (R0 + 5 * sig)) {
    const auto D565 = dv(-0.565, -((R0 - z) / sig));
    const auto D1565 = dv(-1.565, -((R0 - z) / sig));

    const auto frontfac =
        ((exp((-pow(R0 - z, 2)) / (4.0 * pow(sig, 2))) * pow(sig, 0.565)) /
         (1.0 + 0.012 * R0)) *
        phi0;
    const auto bracfac =
        11.26 * D565 / sig + ((0.157 + 11.26 * epsilon) / R0) * D1565;
    return frontfac * bracfac * toGray;
  }
  return 0.0;
}

template <material_t material>
GCC_CONSTEXPR size_t recursive_bethe(std::array<double, n_points> &arr,
                                     double energy, size_t i) {
  if (i < n_points && energy > 0) {
    arr.at(i++) = bethe<material>(energy);
    return recursive_bethe<material>(arr, energy - arr.at(i - 1) * step_length,
                                     i);
  } else {
    return i;
  }
}

template <material_t material>
GCC_CONSTEXPR auto generate_dEdx(const double init_energy) {
  auto vec = std::array<double, n_points>();

  recursive_bethe<material>(vec, init_energy, 0);

  return vec;
}

GCC_CONSTEXPR auto pow_2(const double val) { return val * val; }

GCC_CONSTEXPR double sum_dedx_water(const double depth) {
  constexpr auto dedx = generate_dEdx<WATER>(init_energy);
  auto dedx_sum = 0.0;
  for (size_t j = 0; j < static_cast<size_t>(std::round(depth / step_length));
       ++j) {
    dedx_sum += dedx.at(j);
  }
  return dedx_sum;
}

constexpr auto energy_loss_50mm_water = sum_dedx_water(start_air) * step_length;

GCC_CONSTEXPR double sum_dedx_air(const double depth) {
  constexpr auto dedx =
      generate_dEdx<AIR>(init_energy - energy_loss_50mm_water);
  auto dedx_sum = 0.0;
  for (size_t j = 0; j < static_cast<size_t>(std::round(depth / step_length));
       ++j) {
    dedx_sum += dedx.at(j);
  }
  return dedx_sum;
}

constexpr auto energy_loss_50mm_air = sum_dedx_air(end_air - start_air);

template <bool with_air>
GCC_CONSTEXPR auto
generate_BraggPeak(const std::array<double, n_points> &x_points,
                   const double init_energy, const double weight) {
  auto vec = std::array<double, n_points>();

  const auto phi = 4.0e9 * weight; // Fluence: particles/cm2
  const auto eps = 0.1; // fraction of primary fluence contribution to tail

  constexpr auto p_air = 1.71942633;     // From fit
  constexpr auto alpha_air = 0.00322317; // from fit
  constexpr auto p_water = 1.77;         // exponent of range-energy relation
  constexpr auto alpha_water = 0.0022; // proportionality factor in cm * MeV^-p

  size_t i = 0;
  auto energy = init_energy;
  for (const auto &x_val : x_points) {
    auto p = p_water;
    auto alpha = alpha_water;
    auto z = x_val;

    if (with_air) {
      if (x_val > start_air && x_val < end_air) {
        energy = init_energy - energy_loss_50mm_water;
        alpha = alpha_air;
        p = p_air;
        z = x_val - start_air;
      }
      if (x_val >= end_air) {
        energy = init_energy - energy_loss_50mm_water - energy_loss_50mm_air;
        z = x_val - end_air;
      }

      if (energy <= 0) {
        vec.at(i++) = 0.0;
      }
    }
    const auto R0 = alpha * ce_pow(energy,
                                   p); // approx range in cm
    const auto sigma_mono =
        0.012 * pow(R0, 0.935); // width of gaussian range straggling in cm
    const auto dRdE = alpha * p * ce_pow(energy, p - 1.0);
    const auto sigma_E0 =
        0.01 * energy; // width of gaussian energy spectrum in MeV
    const auto sigma = sqrt(pow_2(sigma_mono) + pow_2(sigma_E0 * dRdE));

    vec.at(i++) = BortfeldBraggPeak(R0, phi, eps, sigma, z / 10.0);
  }

  return vec;
}

GCC_CONSTEXPR auto generate_normed_sobp_weights() {
  std::array<double, 6> weights = {{0.92, 0.35, 0.25, 0.2, 0.16, 0.14}};
  // {1.0, 0.4, 0.25, 0.2, 0.15, 0.1}}; good for -=10 MeV
  auto weight_sum =
      0.0; // std::accumulate(weights.begin(), weights.end(), 0.0);
  for (const auto val : weights) {
    weight_sum += val;
  }

  auto norm_weights = std::array<double, 6>();
  for (size_t i = 0; i < weights.size(); ++i) {
    norm_weights.at(i) = weights.at(i) / weight_sum;
  }

  return norm_weights;
}

template <bool with_air>
GCC_CONSTEXPR auto
generate_array_of_bragg_peaks(const double init_energy,
                              const std::array<double, n_points> &x_points) {
  auto peak_array = std::array<std::array<double, n_points>, 6>();

  GCC_CONSTEXPR auto weights = generate_normed_sobp_weights();
  for (size_t i = 0; i < weights.size(); ++i) {
    peak_array.at(i) = generate_BraggPeak<with_air>(
        x_points, init_energy - i * 5.0, weights.at(i));
  }

  return peak_array;
}

auto array_to_qvector(const std::array<double, n_points> &v_arr) {
  auto Q_points = QVector<double>(v_arr.size());
  std::copy(std::begin(v_arr), std::end(v_arr), Q_points.begin());
  return Q_points;
}

GCC_CONSTEXPR auto generate_x_points() {
  auto x_points = std::array<double, n_points>(); // mm
  // auto i = -step_length;
  for (size_t i = 0; i < n_points; ++i) {
    x_points.at(i) = i * step_length;
  }
  return x_points;
}

GCC_CONSTEXPR
auto arr_p_arr(std::array<double, n_points> &valarr,
               const std::array<double, n_points> arr) {
  for (size_t i = 0; i < n_points; ++i) {
    valarr.at(i) += arr.at(i);
  }
}

GCC_CONSTEXPR
auto arr_div_val(const std::array<double, n_points> &arr, double val) {
  auto arr_out = std::array<double, n_points>();
  for (size_t i = 0; i < n_points; ++i) {
    arr_out.at(i) = arr.at(i) / val;
  }
  return arr_out;
}

GCC_CONSTEXPR
auto arr_div_val_ip(std::array<double, n_points> &arr, double val) {
  // ip = in place
  for (size_t i = 0; i < n_points; ++i) {
    arr.at(i) = arr.at(i) / val;
  }
}

GCC_CONSTEXPR
auto accumulate_arrarr(
    const std::array<std::array<double, n_points>, 6> &peak_array) {
  auto sobp = std::array<double, n_points>();
  for (auto &bragg_peak : peak_array) {
    arr_p_arr(sobp, bragg_peak);
  }
  return sobp;
}

GCC_CONSTEXPR
auto arrarr_div_val(
    const std::array<std::array<double, n_points>, 6> &peak_array,
    const double val) {
  auto out_arr_arr = std::array<std::array<double, n_points>, 6>();
  for (size_t i = 0; i < peak_array.size(); ++i) {
    out_arr_arr.at(i) = arr_div_val(peak_array.at(i), val);
  }
  return out_arr_arr;
}

GCC_CONSTEXPR
auto norm_arr(const std::array<double, n_points> &input_arr) {
  const auto max_val = *std::max_element(input_arr.begin(), input_arr.end());
  return arr_div_val(input_arr, max_val);
}

int main(int argc, char *argv[]) {
  QApplication app(argc, argv);

  GCC_CONSTEXPR auto x_points = generate_x_points();

  GCC_CONSTEXPR
  auto ce_y_points_dEdx = generate_dEdx<WATER>(init_energy);

  auto dedx_with_air = generate_dEdx<WATER>(init_energy);
  GCC_CONSTEXPR auto dedx_mid_air =
      generate_dEdx<AIR>(init_energy - energy_loss_50mm_water);
  GCC_CONSTEXPR auto dedx_end_water = generate_dEdx<WATER>(
      init_energy - energy_loss_50mm_water - energy_loss_50mm_air);

  size_t j = 0;
  size_t k = 0;
  for (size_t i = 0; i < n_points; ++i) {
    const auto pos = i * step_length;
    if (pos > start_air && pos < end_air) {
      dedx_with_air.at(i) = dedx_mid_air.at(j++);
    } else if (pos > end_air) {
      dedx_with_air.at(i) = dedx_end_water.at(k++);
    }
  }

  GCC_CONSTEXPR auto weight = 1.0;

  GCC_CONSTEXPR
  auto ce_y_points_dose_200_air =
      generate_BraggPeak<true>(x_points, init_energy, weight);
  GCC_CONSTEXPR
  auto ce_y_points_dose_200 =
      generate_BraggPeak<false>(x_points, init_energy, weight);

  std::cout << "SOBP time!" << std::endl;
  // Let's make a SOBP of N bragg peaks
  GCC_CONSTEXPR auto ce_peak_array_air =
      generate_array_of_bragg_peaks<true>(init_energy, x_points);
  GCC_CONSTEXPR auto ce_peak_array =
      generate_array_of_bragg_peaks<false>(init_energy, x_points);
  std::cout << "Individual peaks created" << std::endl;

  GCC_CONSTEXPR auto ce_sobp = accumulate_arrarr(ce_peak_array);
  GCC_CONSTEXPR auto sobp = norm_arr(ce_sobp);

  GCC_CONSTEXPR auto ce_sobp_air = accumulate_arrarr(ce_peak_array_air);
  GCC_CONSTEXPR auto sobp_air = norm_arr(ce_sobp_air);

  GCC_CONSTEXPR auto sobp_max =
      *std::max_element(ce_sobp.begin(), ce_sobp.end());
  GCC_CONSTEXPR auto peak_array = arrarr_div_val(ce_peak_array, sobp_max);

  GCC_CONSTEXPR auto sobp_max_air =
      *std::max_element(ce_sobp_air.begin(), ce_sobp_air.end());
  GCC_CONSTEXPR auto peak_array_air =
      arrarr_div_val(ce_peak_array_air, sobp_max_air);

  GCC_CONSTEXPR auto y_points_dose_200 = norm_arr(ce_y_points_dose_200);

  GCC_CONSTEXPR auto y_points_dose_200_air = norm_arr(ce_y_points_dose_200_air);

  std::cout << "SOBP created" << std::endl;

  // "Copy" arrays to QVectors (compiler should make it a memmoved)
  auto Qx_points = array_to_qvector(x_points);
  auto Qy_points_dose_200 = array_to_qvector(y_points_dose_200);
  auto Qy_points_dose_200_air = array_to_qvector(y_points_dose_200_air);
  auto Qy_points_dEdx = array_to_qvector(ce_y_points_dEdx);
  auto Qdedx_with_air = array_to_qvector(dedx_with_air);
  auto Qsobp = array_to_qvector(sobp);
  auto Qsobp_air = array_to_qvector(sobp_air);

  std::cout << "copy complete (mostly)" << std::endl;

  auto customPlot = std::make_unique<QCustomPlot>();
  // create graph and assign data to it:
  int n_graph = 0;

  /// start bragg peak
  auto graph_200 = customPlot->addGraph();
  graph_200->addData(Qx_points, Qy_points_dose_200);
  graph_200->setPen(QPen(Qt::blue));
  graph_200->setName("200 MeV Bragg Peak");

  auto graph_200_air = customPlot->addGraph();
  graph_200_air->addData(Qx_points, Qy_points_dose_200_air);
  graph_200_air->setPen(QPen(Qt::green));
  graph_200_air->setName("200 MeV Bragg Peak with air");

  customPlot->axisRect()->insetLayout()->setInsetAlignment(0, Qt::AlignLeft |
                                                                  Qt::AlignTop);
  customPlot->legend->setVisible(true);

  // give the axes some labels:
  customPlot->xAxis->setLabel("Depth [mm]");
  customPlot->yAxis->setLabel("Dose/max(Dose)");
  // set axes ranges, so we see all data:
  customPlot->xAxis->setRange(0, n_points * step_length);
  customPlot->yAxis->setRange(0, 1.1);

  /// start dE/dx
  ++n_graph;
  customPlot->yAxis2->setVisible(true);
  customPlot->yAxis2->setLabel("dE/dx [keV/µm]");
  // MeV/mm to keV / µm is 1:1
  customPlot->yAxis2->setRange(
      0, *std::max_element(ce_y_points_dEdx.begin(), ce_y_points_dEdx.end()));
  customPlot->yAxis2->setBasePen(QPen(Qt::red));
  customPlot->yAxis2->setLabelColor(Qt::red);
  customPlot->yAxis2->setTickLabelColor(Qt::red);

  auto dedx_graph = customPlot->addGraph(customPlot->xAxis, customPlot->yAxis2);
  dedx_graph->addData(Qx_points, Qy_points_dEdx);
  dedx_graph->setPen(QPen(Qt::red));
  dedx_graph->setName("Stopping power (dE/dx)");
  auto dedx_wa_graph =
      customPlot->addGraph(customPlot->xAxis, customPlot->yAxis2);
  dedx_wa_graph->addData(Qx_points, Qdedx_with_air);
  dedx_wa_graph->setPen(QPen(QBrush(Qt::red), 1, Qt::DashLine));
  dedx_wa_graph->setName("Stopping power with air (dE/dx)");

  customPlot->replot();
  customPlot->savePdf("out_0.pdf");

  /// start weighted peaks
  const auto first_weighted_peak = n_graph + 4;
  // for_each is meant for side effects
  std::for_each(peak_array.begin(), peak_array.end(),
                [&customPlot, &Qx_points](auto vec) {
                  QVector<double> Qvec(vec.size());
                  std::copy(std::begin(vec), std::end(vec), Qvec.begin());
                  auto graph = customPlot->addGraph();
                  graph->addData(Qx_points, Qvec);
                  graph->setPen(QPen(Qt::gray));
                  graph->setName("Weighted Bragg Peaks");
                });

  std::for_each(peak_array.begin(), std::prev(peak_array.end()),
                [&customPlot, first_weighted_peak](auto) {
                  customPlot->legend->removeItem(first_weighted_peak);
                });

  std::for_each(peak_array_air.begin(), peak_array_air.end(),
                [&customPlot, &Qx_points](auto vec) {
                  QVector<double> Qvec(vec.size());
                  std::copy(std::begin(vec), std::end(vec), Qvec.begin());
                  auto graph = customPlot->addGraph();
                  graph->addData(Qx_points, Qvec);
                  graph->setPen(QPen(QBrush(Qt::gray), 1, Qt::DashLine));
                  graph->setName("Weighted Bragg Peaks with air");
                });

  std::for_each(peak_array_air.begin(), std::prev(peak_array_air.end()),
                [&customPlot, first_weighted_peak](auto) {
                  customPlot->legend->removeItem(first_weighted_peak);
                });

  /// start SOBP
  auto sobp_graph = customPlot->addGraph();
  sobp_graph->addData(Qx_points, Qsobp);
  sobp_graph->setPen(QPen(Qt::black));
  sobp_graph->setName("SOBP");

  auto sobp_graph_air = customPlot->addGraph();
  sobp_graph_air->addData(Qx_points, Qsobp_air);
  sobp_graph_air->setPen(QPen(QBrush(Qt::black), 1, Qt::DashLine));
  sobp_graph_air->setName("SOBP with air");

  customPlot->removeGraph(0);
  customPlot->removeGraph(0);

  customPlot->replot();
  std::string out_pdf("out_");
  out_pdf += std::to_string(peak_array.size());
  out_pdf += ".pdf";
  customPlot->savePdf(out_pdf.c_str());

  /// Shifted SOBP
  // Recalc the SOBP with something dense in front

  std::cout << "Done!" << std::endl;

  app.exit();
  return 0;
}
